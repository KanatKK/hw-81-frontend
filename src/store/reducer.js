import {ADD_LINK, ADD_SHORT_URL, ADD_URL} from "./actionTypes";

const initialState = {
    getLink: null,
    url: '',
    shortUrl: '',
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_SHORT_URL:
            return {...state, shortUrl: action.value};
        case ADD_LINK:
            return {...state, getLink: action.value};
        case ADD_URL:
            return {...state, url: action.value};
        default:
            return state;
    }
};

export default reducer;