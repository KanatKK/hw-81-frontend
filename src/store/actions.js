import {ADD_LINK, ADD_SHORT_URL, ADD_URL} from "./actionTypes";
import axios from "axios";

export const addLink = value => {
    return {type: ADD_LINK, value};
};

export const addUrl = value => {
    return {type: ADD_URL, value};
};

export const addShortUrl = value => {
    return {type: ADD_SHORT_URL, value};
};

export const fetchLink = async value => {
    const result = [];
    const sendUrl = async () => {
        const response = await axios.post('http://localhost:8000/link', value);
        await result.push(response.data);
    };
    await sendUrl().catch(console.log);
    if (result.length !== 0) {
        return result;
    }
    return async dispatch => {
        await dispatch(addLink(result))
    };
};