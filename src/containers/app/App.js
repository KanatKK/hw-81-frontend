import React from 'react';
import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {addLink, addShortUrl, addUrl, fetchLink} from "../../store/actions";
import {nanoid} from "nanoid";

const App = () => {
    const dispatch = useDispatch();
    const url = useSelector(state => state.url);
    const shortUrl = useSelector(state => state.shortUrl);
    const reducer = useSelector(state => state)

    const addUrlHandler = event => {
        dispatch(addUrl(event.target.value));
        dispatch(addShortUrl(nanoid(6)));
    };

    const addNewUrl = async () => {
        let result = await fetchLink({url: url, shortUrl: shortUrl});
        dispatch(addLink(result));
    };

    if (reducer.getLink === null) {
        return (
            <div className="container">
                <h1>Shorten your link!</h1>
                <input type="text" value={url} onChange={addUrlHandler} placeholder="Enter URL here" className="linkShorter"/>
                <button type="button" className="short" onClick={addNewUrl}>Shorten!</button>
            </div>
        );
    } else {
        return (
            <div className="container">
                <h1>Shorten your link!</h1>
                <input type="text" value={url} onChange={addUrlHandler} placeholder="Enter URL here" className="linkShorter"/>
                <button type="button" className="short" onClick={addNewUrl}>Shorten!</button>
                <p>Your link now looks like this:</p>
                <a className="shortLink" href={"http://localhost:8000/link/" + reducer.getLink[0].shortUrl}>
                    {"http://localhost:8000/link/" + reducer.getLink[0].shortUrl}
                </a>
            </div>
        )
    }

};

export default App;
